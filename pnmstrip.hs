import Codec.PNM (onPNMs)
import qualified Data.ByteString.Lazy as BS
main = BS.interact (onPNMs (curry id))
