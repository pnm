import Codec.PNM (pnms)
import Codec.PNM.Parse (str)
import qualified Data.ByteString.Lazy as BS
main = BS.interact (BS.concat . map (str . (++"\n") . show . fst) . pnms)
